<?php include 'includes/header.php'; ?>
<div class="hero" style="background-image: url(images/home.jpg)">
	<h1>
		Mums rūpi
	</h1>
	<span class="relative">
		<i class="drop"></i>
	</span>
</div>
<div class="scene-wrap scene-wrap-10">
	<?php include 'includes/lines-3.php'; ?>
</div>
<div class="scene-wrap scene-wrap-11">
	<?php include 'includes/lines-3.php'; ?>
</div>
<div class="scene-wrap scene-wrap-12">
	<?php include 'includes/lines-3.php'; ?>
</div>
<div class="layer layer-blog">
	<div class="wrap">
		<div class="post post-blue">
			<div class="circle big">
				<a href="#" class="sh"></a>
				<div style="background-image: url(images/green.jpg)">
				</div>
			</div>
			<div class="short">
				<div class="in">
					<a href="#" class="sh"></a>
					<h3 class="h3">
						Kiek vandens reikia išgerti per vieną dieną?
					</h3>
					<p>
						Aenean suscipit ex quam, sed eleifend est consectetur id. In pellentesque neq  dui euismod. Integer volutpat aliqua...
					</p>
				</div>
			</div>
		</div>
		<div class="post post-green">
			<div class="circle big">
				<a href="#" class="sh"></a>
				<div style="background-image: url(images/green.jpg)">
				</div>
			</div>
			<div class="short">
				<div class="in">
					<a href="#" class="sh"></a>
					<h3 class="h3">
						Kiek vandens reikia išgerti per vieną dieną?
					</h3>
					<p>
						Aenean suscipit ex quam, sed eleifend est consectetur id. In pellentesque neq  dui euismod. Integer volutpat aliqua...
					</p>
				</div>
			</div>
		</div>
		<div class="post post-orange">
			<div class="circle big">
				<a href="#" class="sh"></a>
				<div style="background-image: url(images/green.jpg)">
				</div>
			</div>
			<div class="short">
				<div class="in">
					<a href="#" class="sh"></a>
					<h3 class="h3">
						Kiek vandens reikia išgerti per vieną dieną?
					</h3>
					<p>
						Aenean suscipit ex quam, sed eleifend est consectetur id. In pellentesque neq  dui euismod. Integer volutpat aliqua...
					</p>
				</div>
			</div>
		</div>
		<div class="post post-blue">
			<div class="circle big">
				<a href="#" class="sh"></a>
				<div style="background-image: url(images/green.jpg)">
				</div>
			</div>
			<div class="short">
				<div class="in">
					<a href="#" class="sh"></a>
					<h3 class="h3">
						Kiek vandens reikia išgerti per vieną dieną?
					</h3>
					<p>
						Aenean suscipit ex quam, sed eleifend est consectetur id. In pellentesque neq  dui euismod. Integer volutpat aliqua...
					</p>
				</div>
			</div>
		</div>
		<div class="post post-green">
			<div class="circle big">
				<a href="#" class="sh"></a>
				<div style="background-image: url(images/green.jpg)">
				</div>
			</div>
			<div class="short">
				<div class="in">
					<a href="#" class="sh"></a>
					<h3 class="h3">
						Kiek vandens reikia išgerti per vieną dieną?
					</h3>
					<p>
						Aenean suscipit ex quam, sed eleifend est consectetur id. In pellentesque neq  dui euismod. Integer volutpat aliqua...
					</p>
				</div>
			</div>
		</div>
		<div class="post post-orange">
			<div class="circle big">
				<a href="#" class="sh"></a>
				<div style="background-image: url(images/green.jpg)">
				</div>
			</div>
			<div class="short">
				<div class="in">
					<a href="#" class="sh"></a>
					<h3 class="h3">
						Kiek vandens reikia išgerti per vieną dieną?
					</h3>
					<p>
						Aenean suscipit ex quam, sed eleifend est consectetur id. In pellentesque neq  dui euismod. Integer volutpat aliqua...
					</p>
				</div>
			</div>
		</div>
		<div class="link-more">
			<span class="relative">
				<i class="drop"></i>
				<a href="#" class="link">Visi įrašai</a>
			</span>
		</div>
	</div>
</div>

<?php include 'includes/footer.php'; ?>