<?php include 'includes/header.php'; ?>
<div class="hero" style="background-image: url(images/produkcija.jpg)">
	<h1>
		Produkcija
	</h1>
	<span class="relative">
		<i class="drop"></i>
	</span>
</div>
<div class="scene-wrap scene-wrap-7">
	<?php include 'includes/lines-1.php'; ?>
</div>
<div class="hooks-production">
	<div class="scene-wrap scene-wrap-18">
		<div class="layer-lines-4"></div>
	</div>
	<div class="scene-wrap scene-wrap-19">
		<div class="layer-lines-5"></div>
	</div>
</div>
<div class="layer layer-products">
	<?php /*
	<i style="position: absolute; z-index:7;top:0;left:0;width:100%;pointer-events: none;height:3000px;opacity:.5;background-image: url(images/b.png); background-size: 100vw auto; background-position: center top;"></i>
	 */ ?>
	<div class="wrap row">
		<div class="filters">
			<div class="filter big">
				<div data-active="negazuotas" class="current">Negazuotas</div>
				<div class="options">
					<div data-val="negazuotas">Negazuotas</div>
					<div data-val="gazuotas">Gazuotas</div>
				</div>
			</div>
			<div class="filter">
				<div data-active="1.5" class="current">1,5 L</div>
				<div class="options">
					<div data-val="0.33">0,33 L</div>
					<div data-val="0.5">0,5 L</div>
					<div data-val="1">1 L </div>
					<div data-val="1.5">1,5 L</div>
					<div data-val="2">2 L</div>
				</div>
			</div>
		</div>
		<div class="minerals"></div>
		<div class="filter-targets">
			<div class="filter-target" data-param-one="negazuotas" data-param-two="1.5">
				<div class="circle circle-offcanvas">
					<div style="background-image: url(images/green.jpg)">
						<img src="images/bottle.png" alt="" />
						<div class="link-holder">
							<a href="#" class="link">Pirk internetu</a>
						</div>
					</div>
				</div>
				<div class="table">
					<h2>
						Analitinė kompozicija (mg/l):
					</h2>
					<div class="cols">
						<div>
							<div>
								Kalcis (Ca2+) <b>49,3</b>
							</div>
							<div>
								Magnis (Mg2+) <b>5,9</b>
							</div>
							<div>
								Natris (Na+) <b>2,3</b>
							</div>
							<div>
								Kalis (K+) <b>0,7</b>
							</div>
							<div>
								Fluoridas (F-) <b>&lt;0,2</b>
							</div>
						</div>
						<div>
							<div>
								Hidrokarbonatas (HCO<div class="up"><div>-</div><div>3</div><!-- &nbsp; reikia palikti -->&nbsp;</div>)
								<b>173,8</b>
							</div>
							<div>
								Chloridas (Cl<div class="up"><div>-</div><!-- &nbsp; reikia palikti -->&nbsp;</div>)
								<b>3,0</b>
							</div>
							<div>
								Sulfatas (HCO<div class="up"><div>2-</div><div>4</div><!-- &nbsp; reikia palikti -->&nbsp;</div>)
								<b>1,0</b>
							</div>
							<div>
								Nitratas (NO<div class="up"><div>-</div><div>3</div><!-- &nbsp; reikia palikti -->&nbsp;</div>)
								<b>0,0</b>
							</div>
							<div>
								Nitritas (NO<div class="up"><div>-</div><div>2</div><!-- &nbsp; reikia palikti -->&nbsp;</div>)
								<b>&lt;0,01</b>
							</div>
						</div>
					</div>
					<p>
						<b>
							Sausa liekana prie 180º: 150 mg/l; ph: 8,0
						</b>
					</p>
					<p>
						Natūralus mineralinis vanduo. Negazuotas. Šaltinis: Nr. 8669 Akvilė, Viečiūnai, Druskininkų sav. Laikyti + 5ºC - +25ºC. Saugoti nuo tiesioginių saulės spindulių. Atidarius laikyti šaldtuve ir suvartoti per 2 dienas. 
					</p>
				</div>
			</div>
			<div class="filter-target" data-param-one="gazuotas" data-param-two="1.5">
				<div class="circle circle-offcanvas">
					<div style="background-image: url(images/green.jpg)">
						<img src="images/bottle.png" alt="" />
						<div class="link-holder">
							<a href="#" class="link">Pirk internetu</a>
						</div>
					</div>
				</div>
				<div class="table">
					<h2>
						Gazuoto Analitinė kompozicija (mg/l):
					</h2>
					<div class="cols">
						<div>
							<div>
								Kalcis (Ca2+) <b>49,3</b>
							</div>
							<div>
								Magnis (Mg2+) <b>5,9</b>
							</div>
							<div>
								Natris (Na+) <b>2,3</b>
							</div>
							<div>
								Kalis (K+) <b>0,7</b>
							</div>
							<div>
								Fluoridas (F-) <b>&lt;0,2</b>
							</div>
						</div>
						<div>
							<div>
								Hidrokarbonatas (HCO<div class="up"><div>-</div><div>3</div><!-- &nbsp; reikia palikti -->&nbsp;</div>)
								<b>173,8</b>
							</div>
							<div>
								Chloridas (Cl<div class="up"><div>-</div><!-- &nbsp; reikia palikti -->&nbsp;</div>)
								<b>3,0</b>
							</div>
							<div>
								Sulfatas (HCO<div class="up"><div>2-</div><div>4</div><!-- &nbsp; reikia palikti -->&nbsp;</div>)
								<b>1,0</b>
							</div>
							<div>
								Nitratas (NO<div class="up"><div>-</div><div>3</div><!-- &nbsp; reikia palikti -->&nbsp;</div>)
								<b>0,0</b>
							</div>
							<div>
								Nitritas (NO<div class="up"><div>-</div><div>2</div><!-- &nbsp; reikia palikti -->&nbsp;</div>)
								<b>&lt;0,01</b>
							</div>
						</div>
					</div>
					<p>
						<b>
							Sausa liekana prie 180º: 150 mg/l; ph: 8,0
						</b>
					</p>
					<p>
						Natūralus mineralinis vanduo. Negazuotas. Šaltinis: Nr. 8669 Akvilė, Viečiūnai, Druskininkų sav. Laikyti + 5ºC - +25ºC. Saugoti nuo tiesioginių saulės spindulių. Atidarius laikyti šaldtuve ir suvartoti per 2 dienas. 
					</p>
				</div>
			</div>
		</div>
		
	</div>
</div>
<?php include 'includes/footer.php'; ?>