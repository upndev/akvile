<?php include 'includes/header.php'; ?>
<div class="hero" style="background-image: url(images/post.jpg)">
	<h1>
		Rūtos Meilutytės fondas
	</h1>
	<span class="relative">
		<i class="drop"></i>
	</span>
</div>
<div class="scene-wrap scene-wrap-14">
	<?php include 'includes/lines-3.php'; ?>
</div>
<div class="scene-wrap scene-wrap-4">
	<?php include 'includes/lines-1.php'; ?>
</div>
<div class="scene-wrap scene-wrap-5">
	<?php include 'includes/lines-1.php'; ?>
</div>
<div class="layer layer-single">
	<div class="wrap">
		<div class="link-back">
			<a href="#" class="link back">Visi įrašai</a>
		</div>
		<div class="single">
			<p>
				Aenean suscipit ex quam, sed eleifend est consectetur id. In pellentesque neq  dui euismod. Integer volutpat aliqua... felis sed sadgem malesuada. Nam eros mauris, luctus ut arcu ac, feugiat pulvinar mi. Aenean suscipit ex quam, sed eleifend est consectetur id. In pellentesque neq  dui euismod. Integer volutpat aliqua... felis sed sadgem malesuada. Nam eros mauris, luctus ut arcu ac, feugiat pulvinar mi.
			</p>
			<p>
				Aenean suscipit ex quam, sed eleifend est consectetur id. In pellentesque neq  dui euismod. Integer volutpat aliqua... felis sed sadgem malesuada. Nam eros mauris, luctus ut arcu ac, feugiat pulvinar mi. Aenean suscipit ex quam, sed eleifend est consectetur id. In pellentesque neq  dui euismod. Integer volutpat aliqua... felis sed sadgem malesuada. Nam eros mauris, luctus ut arcu ac, feugiat pulvinar mi.
			</p>
			<h2>
				<i class="drop"></i>
				<span>
					Rūtos Meilutytės labdaros ir paramos fondas rengia tradicines kalėdines Šeimų plaukimo varžybas
				</span>
			</h2>
			<div class="video">
				<div class="in">
					<div data-yt="w-rv2BQa2OU" id="youtube"></div>
					<i style="background-image: url(images/ruta.jpg)"></i>
					<span id="play" class="play"></span>
					<!--
					<iframe id="youtube" width="560" height="315" src="https://www.youtube.com/embed/w-rv2BQa2OU?controls=0" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					<iframe width="560" height="315" src="https://www.youtube.com/embed/w-rv2BQa2OU" frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
					-->
					<!--
					<a class="play" href="video/video.mp4"></a>
					<div data-video-file="video/video.mp4" class="video-container"></div>
					<i style="background-image: url(images/ruta.jpg)"></i>
					-->
				</div>
			</div>
			<p>
				Aenean suscipit ex quam, sed eleifend est consectetur id. In pellentesque neq  dui euismod. Integer volutpat aliqua... felis sed sadgem malesuada. Nam eros mauris, luctus ut arcu ac, feugiat pulvinar mi. Aenean suscipit ex quam, sed eleifend est consectetur id. In pellentesque neq  dui euismod. Integer volutpat aliqua... felis sed sadgem malesuada. Nam eros mauris, luctus ut arcu ac, feugiat pulvinar mi.
			</p>
			<p>
				Aenean suscipit ex quam, sed eleifend est consectetur id. In pellentesque neq  dui euismod. Integer volutpat aliqua... felis sed sadgem malesuada. Nam eros mauris, luctus ut arcu ac, feugiat pulvinar mi. Aenean suscipit ex quam, sed eleifend est consectetur id. In pellentesque neq  dui euismod. Integer volutpat aliqua... felis sed sadgem malesuada. Nam eros mauris, luctus ut arcu ac, feugiat pulvinar mi.
			</p>
			<div class="gallery row">
				<div class="image">
					<i style="background-image: url(images/ruta.jpg)"></i>
				</div>
				<div class="image">
					<i style="background-image: url(images/ruta.jpg)"></i>
				</div>
				<div class="image">
					<i style="background-image: url(images/ruta.jpg)"></i>
				</div>
				<div class="image">
					<i style="background-image: url(images/ruta.jpg)"></i>
				</div>
			</div>
			<div class="space-140"></div>
			<span class="relative">
				<i class="drop"></i>
			</span>
		</div>
	</div>
</div>

<?php include 'includes/footer.php'; ?>