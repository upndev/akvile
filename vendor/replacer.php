<?php

class Replacer {
	function get($content) {
		$names = ['pxd','pxm'];
		foreach ($names as $name) {
			preg_match('/@'.$name.'(.*?);/', $content, $match);
			for ($i = 0; $i < count($match); $i++) {
				if (strpos($match[$i], ';') !== false) {
					$n = explode(':', $match[$i])[0];
					$m = explode(':', $match[$i])[1];
					$n = str_replace(['@', ' '], '', $n);
					$m = str_replace([' ', ';'], '', $m);
					$content = str_replace('@'.$n, '&&&', $content);
					$content = str_replace($n, ' / '.$m, $content);
					$content = str_replace('&&&', '@'.$n, $content);
				}
			}
		}
		return $content;
	}
}
