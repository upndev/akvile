<?php include 'includes/header.php'; ?>
<div class="hero" style="background-image: url(images/kakes_makes_zaidimas.jpg)">
	<h1>
        Kakės Makės<br>žaidimas
	</h1>
	<span class="relative">
		<i class="drop"></i>
	</span>
</div>
<div class="scene-wrap scene-wrap-6">
	<?php include 'includes/lines-1.php'; ?>
</div>
<div class="layer layer-registration ">
	<div class="wrap">
		<h2 class="h2 small">
            Longer header
		</h2>
		<p>
            Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.
		</p>
		<div class="row row--small">
            <div class="fl-container">
                <?php for ($i=0; $i<5; $i++) : ?>
                    <div class="wrapper">
                        <h3 class="h3">FILTRO PAVADINIMAS</h3>
                        <div class="phone-mockup">
                            <div class="phone-mockup__screen">
                                <div class="phone-mockup__image" style="background-image: url(images/filter-img.jpg)"></div>
                            </div>
                            <div class="phone-mockup__cta">
                                <a target="_blank" href="#" class="phone-mockup__cta-button">IŠBANDYK</a>
                            </div>
                        </div>
                        <?php if ($i%2 == 0) : ?>
                            <div class="mobile-lines big"></div>
                        <?php endif;?>
                </div>
                <?php endfor;?>
            </div>
        </div>

	</div>
</div>

<?php include 'includes/footer.php'; ?>