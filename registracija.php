<?php include 'includes/header.php'; ?>
<div class="hero" style="background-image: url(images/registracija.jpg)">
	<h1>
		Registracija
	</h1>
	<span class="relative">
		<i class="drop"></i>
	</span>
</div>
<div class="scene-wrap scene-wrap-6">
	<?php include 'includes/lines-1.php'; ?>
</div>
<div class="layer layer-registration">
	<div class="wrap">
		<h2 class="h2 small">
			Registracija į žaidimą
		</h2>
		<p>
			Pirkite Akvilės vandenį, registruokitės į žaidimą ir gaukite dovanų originalią spalvinimo knygutę su nykstančiais gyvūnais.
		</p>
		<form class="row">
			<div class="input error">
				<input value="" onkeyup="this.setAttribute('value', this.value);" id="name" name="name" type="text" />
				<label>Vardas</label>
				<div class="error-message">Lorem ipsum dolor</div>
			</div>
			<div class="input">
				<input value="" onkeyup="this.setAttribute('value', this.value);" type="text" />
				<label>Pavardė</label>
				<div class="error-message">Lorem ipsum dolor</div>
			</div>
			<div class="input">
				<input value="" onkeyup="this.setAttribute('value', this.value);" type="email" />
				<label>El. paštas</label>
				<div class="error-message">Lorem ipsum dolor</div>
			</div>
			<div class="input">
				<input value="" onkeyup="this.setAttribute('value', this.value);" type="text" />
				<label>Tel. nr.</label>
				<div class="error-message">Lorem ipsum dolor</div>
			</div>
			<div class="input">
				<input value="" onkeyup="this.setAttribute('value', this.value);" type="text" />
				<label>Adresas</label>
				<div class="error-message">Lorem ipsum dolor</div>
			</div>
			<div class="input">
				<input value="" onkeyup="this.setAttribute('value', this.value);" type="text" />
				<label>Miestas</label>
				<div class="error-message">Lorem ipsum dolor</div>
			</div>
			<div class="input">
				<input value="" onkeyup="this.setAttribute('value', this.value);" type="text" />
				<label>Pašto kodas</label>
				<div class="error-message">Lorem ipsum dolor</div>
			</div>
			<div class="input">
				<input value="" onkeyup="this.setAttribute('value', this.value);" type="text" />
				<label>Kaip dažnai vartojate Akvilės vandenį?</label>
				<div class="error-message">Lorem ipsum dolor</div>
			</div>
			<div class="link pull-right js-validate">Siųsti</div>
		</form>
		<div class="mobile-lines third mobile-lines-6 js-validate"></div>
	</div>
</div>

<?php include 'includes/footer.php'; ?>