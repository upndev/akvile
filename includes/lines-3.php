<svg class="scene" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 1391.3 826.1" xml:space="preserve">
	<?php for ($i = 0; $i < 56; $i++): ?>
	<path transform='translate(0 <?php echo $i * 10;  ?>)' pathdata:id="M0,153.1c0,0,75.8,24.6,199,44
		c123.2,19.4,239.8,16,328-36C653.4,86.5,751.4,53,947,57.1c169.2,3.5,239.8,48.6,400-6" fill="none" stroke="#e5effe" stroke-miterlimit="10" d="M0.3,210.4c0,0,79,35.8,202.2,55.2
		c123.2,19.4,227,19.7,315.2-32.3C644.1,158.6,752.9,12.7,948.4,16.8c169.2,3.5,245.2,38.3,405.3-16.4"/>
	<?php endfor; ?>
</svg>