<?php
$dev = strpos($_SERVER['HTTP_HOST'], 'upndev.com') === false;
if ($dev) {
	require 'vendor/less-compiler/Less.php';
	require 'vendor/replacer.php';
	$content = file_get_contents('less/frontend.less');
	$parser = new Less_Parser([
		'compress'=>true,
		'sourceMap'=>true,
		//'sourceMapWriteTo'=>'css/frontend.min.map',
		//'sourceMapURL'=>'css/frontend.min.map',
	]);
	$content = Replacer::get($content);
	$parser->parse($content);
	$parser->ModifyVars([
		'dir' => '"'.__DIR__.'/../less/"',
		'url' => '"../"'
	]);
	$css = $parser->getCss();
	file_put_contents('css/frontend.min.css',$css);
}

require_once "vendor/Mobile_Detect.php";
$detect = new Mobile_Detect;
if ($detect->isMobile()) {
	$is_mobile = true;
} else {
	$is_mobile = false;
}

?><!DOCTYPE html>
<html itemscope itemtype="http://schema.org/Other">
<head prefix="og: http://ogp.me/ns# profile: http://ogp.me/ns/profile#">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title>Example</title>
	<meta name="robots" content="noindex, nofollow">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<?php /*
	<link rel="icon" href="img/favicon.ico">
	<link rel="canonical" href="http://www.example.com/">
	<meta name="keywords" content="keywords">
	<meta name="description" content="description">
	<meta name="author" content="author">
	<!-- place -->
	<meta name="geo.region" content="LT-VL">
	<meta name="ICBM" content="54.687156, 25.279651">
	<meta name="geo.placename" content="Vilnius">
	<meta name="geo.position" content="54.687156;25.279651">
	<meta name="dc.language" content="lt">
	<!-- itemprop -->
	<meta itemprop="name" content="Example">
	<meta itemprop="description" content="description">
	<meta itemprop="image" content="http://www.example.com/images/example.jpg">
	<!-- image -->
	<meta property="og:image" content="http://www.example.com/images/example.jpg">
	<meta property="og:image:secure_url" content="https://www.example.com/images/example.jpg">
	<meta property="og:image:type" content="image/jpeg">
	<meta property="og:image:width" content="1200">
	<meta property="og:image:height" content="630">
	<!-- og main -->
	<meta property="fb:app_id" content="33333333">
	<meta property="og:title" content="Example">
	<meta property="og:description" content="description">
	<meta property="og:type" content="profile">
	<meta property="og:site_name" content="Example Parent">
	<meta property="og:locale" content="lt_LT">
	<meta property="og:locale:alternate" content="en_US">
	<meta property="og:locale:alternate" content="ru_RU">
	<meta property="og:url" content="http://www.example.com/">
	<!-- video -->
	<meta property="og:video" content="http://example.com/movie.swf" />
	<meta property="og:video:secure_url" content="https://secure.example.com/movie.swf" />
	<meta property="og:video:type" content="application/x-shockwave-flash" />
	<meta property="og:video:width" content="400" />
	<meta property="og:video:height" content="300" />
	<!-- audio -->
	<meta property="og:audio" content="http://example.com/sound.mp3" />
	<meta property="og:audio:secure_url" content="https://secure.example.com/sound.mp3" />
	<meta property="og:audio:type" content="audio/mpeg" />
	<!-- twitter -->
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:site" content="@mytwitter" />
	<meta name="twitter:title" content="Example" />
	<meta name="twitter:description" content="description" />
	<meta name="twitter:image" content="https://www.example.com/images/example.jpg" />
	<meta name="twitter:url" content="https://www.example.com/" />
	<!-- apple -->
	<link rel="apple-touch-icon" href="touch-icon-iphone.png"> <!-- 60 x 60 -->
	<link rel="apple-touch-icon" sizes="76x76" href="touch-icon-ipad.png">
	<link rel="apple-touch-icon" sizes="120x120" href="touch-icon-iphone-retina.png">
	<link rel="apple-touch-icon" sizes="152x152" href="touch-icon-ipad-retina.png">
	<link rel="apple-touch-startup-image" href="startup.png" /> <!-- 320 x 480 -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<meta name="apple-mobile-web-app-status-bar-style" content="black" />
	<!-- android -->
	<link rel="shortcut icon" sizes="196x196" href="touch-icon-android-retina.png">
	<link rel="shortcut icon" sizes="128x128" href="touch-icon-android.png">
	*/ ?>
	<script>(function(w){var dpr=((w.devicePixelRatio===undefined)?1:w.devicePixelRatio);if(!!w.navigator.standalone){var r=new XMLHttpRequest();r.open('GET','/retinaimages.php?devicePixelRatio='+dpr,false);r.send()}else{document.cookie='devicePixelRatio='+dpr+'; path=/'}})(window)</script>
	<noscript><style id="devicePixelRatio" media="only screen and (-moz-min-device-pixel-ratio: 2), only screen and (-o-min-device-pixel-ratio: 2/1), only screen and (-webkit-min-device-pixel-ratio: 2), only screen and (min-device-pixel-ratio: 2)">html{background-image:url("/retinaimages.php?devicePixelRatio=2")}</style></noscript>
	
	<link rel="stylesheet" type="text/css" href="css/frontend.min.css">
</head>
<body>
	<script>
		var is_mobile = <?php echo $is_mobile ? 'true' : 'false'; ?>;
	</script>
	<div id="page">
			
	<div class="header">
		<div class="bars">
			<svg viewBox="0 0 24 24">
			    <path d="M3,6H21V8H3V6M3,11H21V13H3V11M3,16H21V18H3V16Z" />
			</svg>
			<svg viewBox="0 0 24 24">
			    <path d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z" />
			</svg>
		</div>
		<a href="#" class="logo">
			<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 298.1 82.6" enable-background="new 0 0 298.1 82.6" xml:space="preserve"><g><defs><path id="SVGID_1_" d="M170.2,22.5c-1.8,0-3.3,0.4-4.6,1.1c-1.3,0.7-2.4,1.6-3.2,2.7c-0.8,1.1-1.3,2.1-1.5,3.2c-0.7,3.7-1.6,7.4-2.6,11.2c-2.2,8.2-5.3,16.8-9.3,24.4c-4-7.5-7.1-16.2-9.3-24.4c-1-3.8-1.9-7.5-2.6-11.2c-0.2-1.1-0.7-2.2-1.5-3.2c-0.8-1.1-1.9-2-3.2-2.7c-1.3-0.7-2.8-1.1-4.6-1.1h-8.3c0.2,2.7,0.7,5.6,1.4,8.7c0.7,3.1,1.6,6.4,2.6,9.7c1.1,3.3,2.2,6.7,3.5,10c1.3,3.3,2.7,6.6,4.1,9.7c1.4,3.1,2.9,6.1,4.4,8.8c1.5,2.7,2.9,5.1,4.3,7.1c1,1.5,2,2.6,3,3.5c1,0.9,2,1.5,3.2,1.9c0.9,0.3,1.8,0.5,2.8,0.5c1-0.1,2-0.2,2.9-0.5c1.1-0.4,2.2-1,3.2-1.9c1-0.9,2-2,3-3.5c1.4-2.1,2.9-4.4,4.4-7.1c1.5-2.7,2.9-5.6,4.4-8.8c1.4-3.1,2.8-6.4,4.1-9.7c1.3-3.4,2.5-6.7,3.5-10c1.1-3.4,1.9-6.6,2.6-9.7c0.7-3.1,1.2-6,1.4-8.7H170.2z M186.3,22.5v42.2c0,6,1.6,10.6,4.6,13.7c2.6,2.7,5.7,4.1,10,4.1c4.3,0,9.2-2.6,9.2-8.4v-3c-1.7,0-3.6-0.1-5.2-0.7c-1.3-0.5-2.3-1.3-2.9-2.5c-0.6-1.1-0.9-2.7-0.9-4.7V33.5c0-3.7-1-6.5-2.8-8.3c-1.8-1.8-4.4-2.7-7.8-2.7H186.3z M17.8,68.8c-1.8-1.3-2.7-3.1-2.7-5.3c0-1.6,0.4-3,1.2-4c0.9-1.1,2.3-1.9,4.4-2.5c2.1-0.6,5-0.8,8.7-0.8h5.3v5.6c0,3-0.8,5.2-2.3,6.7c-1.6,1.5-3.9,2.3-7.1,2.3C22.1,70.7,19.6,70.1,17.8,68.8 M24.1,21.9c-3.9,0-7.2,0.5-10,1.3c-2.8,0.9-5,2-6.7,3.4c-1.7,1.4-3,2.9-3.8,4.6c-0.8,1.7-1.2,3.3-1.2,5v4.7h0.6c0.6-0.7,1.3-1.4,2.4-2.2c1-0.8,2.3-1.5,3.9-2.2c1.5-0.7,3.4-1.2,5.5-1.7c2.1-0.4,4.5-0.6,7.1-0.7c2.9,0,5.3,0.3,7.2,1c1.9,0.6,3.3,1.8,4.2,3.3c0.9,1.6,1.4,3.7,1.4,6.5v0.8h-5.2c-5.4,0-10,0.5-13.8,1.4c-3.8,0.9-6.8,2.2-9.1,3.9c-2.3,1.6-4,3.6-5.1,5.8C0.5,58.9,0,61.4,0,64c0,2.7,0.6,5.2,1.7,7.4c1.1,2.3,2.8,4.2,4.9,5.9c2.1,1.7,4.6,2.9,7.5,3.9c2.9,0.9,6.1,1.4,9.6,1.4h3c3.3,0,6.3-0.5,9.1-1.5c2.7-1,5.1-2.4,7.1-4.3c2-1.8,3.5-4.1,4.6-6.6c1.1-2.6,1.6-5.4,1.7-8.5V48.1c0-5-0.7-9.2-2.1-12.6c-1.4-3.4-3.2-6.1-5.5-8.1c-2.3-2-5-3.4-8-4.3c-2.9-0.9-6-1.3-9.3-1.3H24.1z M263.3,42.7c0-3,0.8-5.2,2.3-6.8c1.6-1.5,3.9-2.3,7.1-2.3c3.3,0,5.8,0.7,7.6,1.9c1.8,1.3,2.7,3.1,2.7,5.4c0,1.6-0.4,2.9-1.2,4c-0.9,1.1-2.3,1.9-4.4,2.5c-2.1,0.6-5,0.8-8.7,0.8h-5.3V42.7z M271.3,21.9c-3.3,0-6.3,0.5-9.1,1.5c-2.7,1-5.1,2.4-7.1,4.3c-2,1.8-3.5,4-4.6,6.6c-1.1,2.6-1.6,5.4-1.7,8.5v13.5c0,5,0.7,9.2,2.1,12.6c1.4,3.4,3.2,6.1,5.5,8.1c2.3,2,5,3.4,8,4.3c3,0.9,6.2,1.3,9.5,1.3c3.9,0,7.2-0.5,10-1.3c2.8-0.9,5-2,6.7-3.4c1.7-1.4,3-3,3.8-4.6c0.8-1.7,1.2-3.3,1.2-5v-4.7H295c-0.6,0.7-1.3,1.4-2.4,2.2c-1,0.8-2.3,1.5-3.9,2.2c-1.5,0.7-3.4,1.3-5.4,1.7c-2.1,0.4-4.5,0.6-7.1,0.7c-2.9,0-5.3-0.3-7.2-1c-1.9-0.7-3.3-1.8-4.2-3.3c-0.9-1.6-1.3-3.7-1.3-6.5v-0.8h5.2c5.4,0,10-0.5,13.8-1.4c3.8-0.9,6.8-2.2,9.1-3.9c2.3-1.6,4-3.6,5.1-5.8c1-2.2,1.6-4.6,1.5-7.3c0-2.7-0.6-5.2-1.7-7.4c-1.2-2.3-2.8-4.2-4.9-5.9c-2.1-1.7-4.6-2.9-7.5-3.9c-2.9-0.9-6.1-1.4-9.7-1.4H271.3z M61.5,0v71.5c0,3.5,0.8,6,2.5,7.8c1.7,1.8,4.2,2.6,7.6,2.6l4.7,0v-29l10,13.2c4.2,5.6,8.4,11.1,14.6,14.5c1.1,0.6,2.2,1.1,3.4,1.4c1.1,0.3,2.3,0.5,3.4,0.5c2,0,3.6-0.3,5-0.8c1.3-0.5,2.3-1.4,3.1-2.5c0.8-1.1,1.4-2.4,1.9-4c-1.2-0.4-2.3-0.8-3.4-1.3c-1.1-0.5-2.3-1.3-3.5-2.2c-1.3-1-2.8-2.3-4.6-4.1c-2.6-2.7-5.2-5.8-7.5-8.7c-2.4-2.9-4.8-5.9-7.1-8.8c1.3-1.6,2.8-3.4,4.5-5.5c1.7-2,3.5-4.2,5.5-6.4c1.9-2.2,3.8-4.3,5.7-6.4c1.8-2,3.5-3.8,5-5.4c1.5-1.5,2.7-2.7,3.5-3.4v-0.5h-9.1c-1.6,0-3,0.1-4.4,0.3c-1.4,0.2-2.6,0.6-3.9,1.1c-5.1,2.4-9.7,8.7-13.1,12.9l-9,11.2V9.8c0-1.9-0.3-3.6-1-5.1c-0.7-1.5-1.8-2.6-3.3-3.5C70.5,0.4,68.6,0,66.2,0H61.5z M189.6,1.1c-1.2,0.7-2.2,1.7-2.9,2.9c-0.7,1.2-1.1,2.6-1.1,4.1c0,1.5,0.4,2.9,1.1,4.1c0.7,1.2,1.7,2.2,2.9,3c1.2,0.7,2.6,1.1,4.1,1.1c1.5,0,2.9-0.4,4.1-1.1c1.2-0.7,2.2-1.7,2.9-3c0.7-1.2,1.1-2.6,1.1-4.1c0-1.5-0.4-2.9-1.1-4.1c-0.7-1.2-1.7-2.2-2.9-2.9c-1.2-0.7-2.6-1.1-4.1-1.1C192.1,0,190.8,0.4,189.6,1.1 M217.6,0v64.7c0,6,1.6,10.6,4.6,13.7c2.6,2.7,5.7,4.1,10,4.1c4.3,0,9.2-2.6,9.2-8.4v-3c-1.7,0-3.6-0.1-5.2-0.7c-1.3-0.5-2.3-1.3-2.9-2.5c-0.6-1.1-0.9-2.7-0.9-4.7V11c0-3.7-1-6.5-2.8-8.3c-1.8-1.8-4.4-2.7-7.8-2.7H217.6z M269.4,1.1c-1.2,0.7-2.2,1.7-2.9,2.9c-0.7,1.2-1.1,2.6-1.1,4.1c0,1.5,0.4,2.9,1.1,4.1c0.7,1.2,1.7,2.2,2.9,3c1.2,0.7,2.6,1.1,4.1,1.1c1.5,0,2.9-0.4,4.1-1.1c1.2-0.7,2.2-1.7,2.9-3c0.7-1.2,1.1-2.6,1.1-4.1c0-1.5-0.4-2.9-1.1-4.1c-0.7-1.2-1.7-2.2-2.9-2.9c-1.2-0.7-2.6-1.1-4.1-1.1C271.9,0,270.6,0.4,269.4,1.1"/></defs><clipPath id="SVGID_2_"><use xlink:href="#SVGID_1_" overflow="visible"/></clipPath><linearGradient id="SVGID_3_" gradientUnits="userSpaceOnUse" x1="0" y1="82.564" x2="1" y2="82.564" gradientTransform="matrix(0 -84.3778 -298.1063 0 24761.9102 84.3357)"><stop offset="0" style="stop-color:#0033A0"/><stop offset="1" style="stop-color:#00AEEF"/></linearGradient><rect y="0" clip-path="url(#SVGID_2_)" fill="url(#SVGID_3_)" width="298.1" height="82.6"/></g></svg>
		</a>
		<div class="nav-bg"></div>
		<nav>
			<ul>
                <li>
                    <a href="./kakes_makes_zaidimas.php">
                        Kakės Makės žaidimas
                    </a>
                </li>
				<li>
					<a href="#">
						Registracija į žaidimą
					</a>
				</li>
				<li>
					<a href="#">
						Gavyba
					</a>
				</li>
				<li>
					<a href="#">
						Produkcija
					</a>
				</li>
				<li>
					<a href="#">
						Mums rūpi
					</a>
				</li>
				<li>
					<a href="#">
						Kontaktai
					</a>
				</li>
			</ul>
		</nav>
	</div>