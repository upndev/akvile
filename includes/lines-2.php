<svg class="scene" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
	 viewBox="0 0 1325.1 461.8" enable-background="new 0 0 1325.1 461.8" xml:space="preserve">
	<?php for ($i = 0; $i < 60; $i++): ?>
	<path transform='translate(0 <?php echo $i * 8; ?>)' pathdata:id="M0,24.8c0,0,78.2,19.4,202.5,29.7c100.5,8.4,231,3.8,338.3-15C690.5,13.3,800,2,929.5,8.2
		c169,8.1,220.5,10.1,395.4-7.3" fill="none" stroke="#e5effe" stroke-miterlimit="10" d="M0.3,6.3c0,0,79,35.6,202.2,55.2
		c96.6,15.4,244,25,338.3-15C680.7-12.9,803.1-4.6,929.5,15.2c167.2,26.2,235.3,47.3,395.4-7.3"/>
	<?php endfor; ?>
</svg>