var $d = $(document), tl, $w = $(window), $schema = $('.schema'), notPlayed = 1,
	$hero = $('.hero'), heroHeight = 0, $b = $('body'), ww = $w.width(),
	$scenes = $('svg.scene'), enabled = false, playingElement = false, YTplayer,
	setLayouts = debounce(function() {
		heroHeight = $hero.height();
		ww = $w.width();
	},0);

/*
document.addEventListener('touchmove', function (e) { e.preventDefault(); }, isPassive() ? {
	capture: false,
	passive: false
} : false);
*/
if ($('#youtube').length) {
	var tag = document.createElement('script');
	tag.src = "//www.youtube.com/player_api";
	var firstScriptTag = document.getElementsByTagName('script')[0];
	firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);
}

$(function(){

	FastClick.attach(document.body);
	checkFilters();
	setLayouts();
	checkInputs();
	if (!is_mobile) {
		$('[data-hero-video]').each(function(){
			var video = $(this).attr('data-hero-video');
			$(this).vide(video, {
				loop: true,
				muted: true,
				autoplay: true,
			});
		});
		$('[data-video-file]').each(function(){
			var video = $(this).attr('data-video-file');
			$(this).vide(video, {
				loop: true,
				muted: true,
				autoplay: true,
			});
		});
	}
   	var $circle1 = $('.schema-circle-1');
   	if ($circle1.length) {
	   	var $circle2 = $('.schema-circle-2');
	   	var $circle3 = $('.schema-circle-3');
	   	var $circle = $('.schema-circle-main');
	   	var $txt = $('.txt');
	   	tl = new TimelineMax();
	   	tl.timeScale(0.5);
	   	tl
   		.to($txt.eq(0), 0.3, {x: 0, opacity: 1})
//   		.to($circle, 0.4, {rotation: 152.8, delay: 0.3})
   		.to($circle, 0.4, {rotation: 153.05, delay: 0.3})
   		.to($txt.eq(1), 0.3, {x: 0, opacity: 1})
   		.to($circle, 0, {rotation: 129, delay: 0.3})
   		.set($circle1, {display:'block'})
   		.to($circle, 0.4, {rotation: 102.6})
   		.to($txt.eq(2), 0.3, {y: 0, opacity: 1})
   		.to($circle, 0, {rotation: 79, delay: 0.3})
   		.set($circle2, {display:'block'})
   		.to($circle, 0.4, {rotation: 53.1})
   		.to($txt.eq(3), 0.3, {x: 0, opacity: 1})
   		.to($circle, 0, {rotation: 29, delay: 0.3})
   		.set($circle3, {display:'block'})
   		.to($circle, 0.4, {rotation: 12.9})
   		.to($txt.eq(4), 0.3, {x: 0, opacity: 1});
   		tl.stop();
   }
   setTimeout(function(){
   		onScroll();
   },10);
   
	window.sr = ScrollReveal({ duration: 1000 });
	sr.reveal('.water-png, .filter-targets', {
		opacity: 0,
		scale: 1,
		distance: '1rem',
	});
	sr.reveal('.layer-blog .post', {
		opacity: 0,
		scale: 1,
		distance: '1rem',
	});
	/*
	sr.reveal('.layer-blog .post:nth-child(odd) .circle', {
		origin: 'left',
		distance: '1rem',
	});
	sr.reveal('.layer-blog .post:nth-child(even) .circle', {
		origin: 'right',
		distance: '1rem',
	});
	*/
	
	
});

$(window).resize(setLayouts);

$(window).scroll(function(){
	onScroll();
	if (playingElement) {
		$(playingElement).prop('muted', true);
		playingElement = false;
	}
});

$(window).on('DOMContentLoaded load resize scroll', function(){
	if ($scenes.length) {
		if (!enabled && !is_mobile) {
			var is = false;
			$scenes.each(function(){
				if (isElementPartiallyInViewport($(this))) {
					is = true;
				}
			});
			if (is) {
				enabled = true;
				setTimeout(function(){
					animeSvg();
				},800);
			}
		}
		
	}
});

function isElementPartiallyInViewport(el){
    //special bonus for those using jQuery
    if (typeof jQuery !== 'undefined' && el instanceof jQuery) el = el[0];

    var rect = el.getBoundingClientRect();
    // DOMRect { x: 8, y: 8, width: 100, height: 100, top: 8, right: 108, bottom: 108, left: 8 }
    var windowHeight = (window.innerHeight || document.documentElement.clientHeight);
    var windowWidth = (window.innerWidth || document.documentElement.clientWidth);

    // http://stackoverflow.com/questions/325933/determine-whether-two-date-ranges-overlap
    var vertInView = (rect.top <= windowHeight) && ((rect.top + rect.height) >= 0);
    var horInView = (rect.left <= windowWidth) && ((rect.left + rect.width) >= 0);

    return (vertInView && horInView);
}

function inViewport (el) {

    var r, html;
    if ( !el || 1 !== el.nodeType ) { return false; }
    html = document.documentElement;
    r = el.getBoundingClientRect();

    return ( !!r 
      && r.bottom >= 0 
      && r.right >= 0 
      && r.top <= html.clientHeight 
      && r.left <= html.clientWidth 
    );

}

// debounce
function debounce(func, wait, immediate) {
	var timeout;
	return function() {
		var context = this, args = arguments;
		var later = function() {
			timeout = null;
			if (!immediate) func.apply(context, args);
		};
		var callNow = immediate && !timeout;
		clearTimeout(timeout);
		timeout = setTimeout(later, wait);
		if (callNow) func.apply(context, args);
	}
}

// ref https://github.com/WICG/EventListenerOptions/pull/30
function isPassive() {
    var supportsPassiveOption = false;
    try {
        addEventListener("test", null, Object.defineProperty({}, 'passive', {
            get: function () {
                supportsPassiveOption = true;
            }
        }));
    } catch(e) {}
    return supportsPassiveOption;
}

$d

.on('click','.video .play',function(e){
	/*
	if (is_mobile) {
		return true;
	} else {
		e.preventDefault();
		var $block = $(this).closest('.video');
		$block.addClass('playing');
		var video = $block.find('video').get(0);
		video.play();
		playingElement = video;
		$(playingElement).prop('muted', false);
	}
	*/
	e.preventDefault();
	var $block = $(this).closest('.video');
	if ($block.hasClass('playing')) {
		$block.removeClass('playing');
		if (YTplayer) {
			YTplayer.pauseVideo();
		}
	} else {
		$block.addClass('playing');
		if (YTplayer) {
			YTplayer.playVideo();
		}
	}
	
})
.on('click','.error input',function(e){
	$(this).closest('.input').removeClass('error');
})
.on('click','.js-validate',function(e){
	var valid = true;
	$('form input').each(function(){
		if (!$(this).val() || ($(this).attr('type') == 'email' && !validateEmail($(this).val()))) {
			valid = false;
			$(this).closest('.input').addClass('error');
		}
	});
	if (valid) {
		// submit form
	}
})
.on('click','.playing',function(e){
	$(this).find('video').get(0).pause();
	playingElement = null;
	$(this).removeClass('playing');
})
.on('click','.bars',function(e){
	$b.toggleClass('menu-open');
})
.on('click','.mobile-schema',function(e){
	$b.addClass('schema-show');
})
.on('click','.schema-close',function(e){
	$b.removeClass('schema-show');
})
.on('click','.circle .play',function(e){
	$(this).closest('.circle > div').mouseenter();
})
.on('click','.intro-drop',function(e){
	$('body, html').animate({scrollTop: $('.hero').height()},750);
})
.on('mouseenter','.circle > div',function(e){
	var $el = $(this);
	var $video = $(this).find('video');
	if ($video.length) {
		var promise = $video.get(0).play();
		$video.prop('muted', false);
		/*
		var promise = $video.get(0).play();
		if (promise !== undefined) {
		  	promise.then(function(){
		  		$el.addClass('enter');
		  		$el.find('.play').hide();
		  	}).catch(function(){
		  		if (!$el.find('.play').length) {
			  		$el.append('<span class="play"></span>');
		  		}
		  		$el.find('.play').show();
		  	});
		}
		*/
	}
})
.on('mouseleave','.circle > div',function(e){
	$(this).removeClass('enter');
	var $el = $(this);
	var $video = $(this).find('video');
	$el.find('.play').hide();
	if ($video.length) {
		//$video.get(0).pause();
		$video.prop('muted', true);
	}
})
.on('click','.bubble',function(e){
	if (!$(this).hasClass('open')) {
		$('.bubble').removeClass('open');
	}
	$(this).toggleClass('open');
})
.on('click','html',function(e){
	if (!$(e.target).closest('.filter').length) {
		$('.filter').removeClass('open');
	}
	if (!$(e.target).closest('.bubble').length) {
		$('.bubble').removeClass('open');
	}
	
})
.on('click','.filter .current',function(){
	$(this).closest('.filter').toggleClass('open');
})
.on('click','.filter .options > div',function(){
	var current = $(this).attr('data-val');
	var text = $(this).text();
	$(this).closest('.filter').removeClass('open')
		.find('.current').attr('data-active',current)
		.text(text);
	checkFilters();
})
;

function checkFilters() {
	$('.filter').each(function(){
		var current = $(this).find('[data-active]').attr('data-active');
		$(this).find('[data-val]').show()
		$(this).find('[data-val="'+current+'"]').hide();
	});
	$('.filter-target').removeClass('active');
	var current1 = $('[data-active]').eq(0).attr('data-active');
	var current2 = $('[data-active]').eq(1).attr('data-active');
	$('.filter-target[data-param-one="'+current1+'"][data-param-two="'+current2+'"]').addClass('active');
}


/**
 * demo3.js
 * http://www.codrops.com
 *
 * Licensed under the MIT license.
 * http://www.opensource.org/licenses/mit-license.php
 * 
 * Copyright 2017, Codrops
 * http://www.codrops.com
 */
/*
{
	class MorphingBG {
		constructor(el) {
			this.DOM = {};
			this.DOM.el = el;
			this.DOM.paths = Array.from(this.DOM.el.querySelectorAll('path'));
			this.animate();
		}
		animate() {
			this.DOM.paths.forEach((path) => {
				setTimeout(() => {
					anime({
						targets: path,
						duration: 4000,//anime.random(3000,5000),
						easing: [0.5,0,0.5,1],
						d: path.getAttribute('pathdata:id'),
						loop: true,
						direction: 'alternate'
					});
//				}, anime.random(0,1000));
				}, 0);
			});
		}
	};

	new MorphingBG(document.querySelector('svg.scene'));
};
*/

function animeSvg() {
	$('svg.scene').each(function(){
		$(this).find('path').each(function(){
			anime({
				targets: this,
				duration: 4000,//anime.random(3000,5000),
				easing: [0.5,0,0.5,1],
				d: this.getAttribute('pathdata:id'),
				loop: true,
				direction: 'alternate'
			});
		});
	})
}

function onScroll() {
	var fromTop = $w.scrollTop();
	if (fromTop > 10) {
		$b.addClass('scrolled');
	} else {
		$b.removeClass('scrolled');
	}
	if ($schema.length && notPlayed) {
		if (fromTop > $schema.offset().top - ($w.height() - $schema.height()) / 4 * 3) {
			notPlayed = false;
			tl.play();
		}
	}
	if (heroHeight && is_mobile) {
		if (fromTop > heroHeight) {
			$b.addClass('mobile-scrolled');
		} else {
			$b.removeClass('mobile-scrolled');
		}
	}
	if (heroHeight) {
		if (fromTop > ww * 0.1) {
			$b.addClass('intro-scrolled');
		} else {
			$b.removeClass('intro-scrolled');
		}
	}
}

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function checkInputs() {
	$('input, textarea').attr('autocorrect','off')
		.attr('autocapitalize','off')
		.attr('spellcheck','false');
	autosize($('textarea'));
}

function onYouTubePlayerAPIReady() {
  	YTplayer = new YT.Player('youtube', {
  		videoId: $('#youtube').attr('data-yt'),
  		playerVars: {
	        'autoplay': 1,
	        'controls': 1, 
	        'autohide': 1,
	        'showinfo' : 0, // <- This part here
	        'wmode': 'opaque',
	        'rel': 0,
	        'loop': 1
	    },
    	events: {
      		'onReady': onPlayerReady
    	}
  	});
}

function onPlayerReady(event) {
	/*
	var playButton = document.getElementById("play");
  	playButton.addEventListener("click", function() {
    	YTplayer.playVideo();
  	});
  	var playButton = document.getElementById(".play");
  	playButton.addEventListener("click", function() {
    	YTplayer.playVideo();
  	});
  
  	var pauseButton = document.getElementById("pause-button");
  	pauseButton.addEventListener("click", function() {
    	YTplayer.pauseVideo();
  	});
  	*/
}
