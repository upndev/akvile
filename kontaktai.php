<?php include 'includes/header.php'; ?>
<div class="hero" style="background-image: url(images/contacts.jpg)">
	<h1>
		Kontaktai
	</h1>
	<span class="relative">
		<i class="drop"></i>
	</span>
</div>
<div class="scene-wrap scene-wrap-6">
	<?php include 'includes/lines-1.php'; ?>
</div>
<div class="layer layer-contacts">
	<div class="wrap">
		<h2 class="h2 small">
			Užklausos forma
		</h2>
		<form class="row">
			<div class="input">
				<input value="" onkeyup="this.setAttribute('value', this.value);" type="text" />
				<label>Vardas</label>
				<div class="error-message">Lorem ipsum dolor</div>
			</div>
			<div class="input">
				<input value="" onkeyup="this.setAttribute('value', this.value);" type="text" />
				<label>Pavardė</label>
				<div class="error-message">Lorem ipsum dolor</div>
			</div>
			<div class="input">
				<input value="" onkeyup="this.setAttribute('value', this.value);" type="email" />
				<label>El. paštas</label>
				<div class="error-message">Lorem ipsum dolor</div>
			</div>
			<div class="input">
				<input value="" onkeyup="this.setAttribute('value', this.value);" type="text" />
				<label>Tel. nr.</label>
				<div class="error-message">Lorem ipsum dolor</div>
			</div>
			<div class="input full">
				<textarea value="" onkeyup="this.setAttribute('value', this.value);"></textarea>
				<label>Jūsų žinutė:</label>
				<div class="error-message">Lorem ipsum dolor</div>
			</div>
			<div class="link pull-right js-validate">Siųsti</div>
		</form>
		<div class="mobile-lines third mobile-lines-6"></div>
		<div class="phone">
			<span class="relative">
				<i class="drop"></i>
				<span>
					Kokybės linija:
					<b>
						+370 527 86318
					</b>
				</span>
			</span>
		</div>
	</div>
</div>

<?php include 'includes/footer.php'; ?>