<?php include 'includes/header.php'; ?>
<div class="hero big" data-hero-video="video/Akvile-forest-v2_FB_Test.mp4">
	<i class="desktop-hide" style="background-image: url(images/home.jpg)"></i>
	<h1>
		Ateities vanduo
	</h1>
</div>
<div class="scene-wrap scene-wrap-8">
	<?php include 'includes/lines-2.php'; ?>
</div>
<div class="hooks">
	<div class="scene-wrap scene-wrap-15">
		<div class="layer-lines-1"></div>
	</div>
	<div class="scene-wrap scene-wrap-16">
		<div class="layer-lines-2"></div>
	</div>
	<div class="scene-wrap scene-wrap-17">
		<div class="layer-lines-3"></div>
	</div>
</div>
<div class="layer layer-intro">
	<?php /*
	<i style="display: none;position: absolute;top:0;left:0;width:100%;pointer-events: none;height:3000px;opacity:.5;background-image: url(images/a.png); background-size: 100vw auto; background-position: center top;"></i>
	 */ ?>
	<div class="wrap">
		<h2 class="h2">
			<div class="intro-drop"><i class="drop">
				<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 58.8 92.1"><defs><path id="a" d="M0 0h58.8v92.1H0z"/></defs><clipPath id="b"><use xlink:href="#a" overflow="visible"/></clipPath><path clip-path="url(#b)"  d="M56 50.3c-.4-.8-.8-1.6-1.3-2.4L29.4 0 4.8 46.7c-1 1.6-1.9 3-2.6 4.8v-.2C.8 55.3 0 58.7 0 62.6c0 16.2 13.2 29.5 29.4 29.5s29.4-13.2 29.4-29.4c0-4.4-1-8.4-2.8-12.4"/></svg>
			</i></div>
			<span>
				Natūralus mineralinis vanduo
			</span>
		</h2>
		<div class="wrap-intro">
			<div class="bubble bubble-1">
				<span>pH 8,0</span>
				<svg class="text" viewBox="64.7 289.8 312.7 104.8">
					<path id="curve" fill="none" d="M64.7,393.1c3.5-6.6,57.9-104.5,157.9-103.2c98.4,1.3,151,97.5,154.8,104.7"/>
		            <text width="100" fill="#fff">
		                <textPath xlink:href="#curve">
		                    &nbsp;ŠARMINIS
		                </textPath>
		            </text>
		        </svg>
		        <div class="tip">
		        	Lorem ipsum dolor sit amet, con sectetur adipiscing elit. Ut tin cid unt arcu orci, tempor pulvinar nibh accumsan non. Nam pulvinar, just non rutrum lacinia, dolor magna
		        </div>
			</div>
			<div class="bubble bubble-2">
				<svg class="text" viewBox="64.7 289.8 312.7 104.8">
					<path id="curve" fill="none" d="M64.7,393.1c3.5-6.6,57.9-104.5,157.9-103.2c98.4,1.3,151,97.5,154.8,104.7"/>
		            <text width="100" fill="#fff">
		                <textPath xlink:href="#curve">
		                    KŪDIKIAMS
		                </textPath>
		            </text>
		        </svg>
		        <div class="tip">
		        	Lorem ipsum dolor sit amet, con sectetur adipiscing elit. Ut tin cid unt arcu orci, tempor pulvinar nibh accumsan non. Nam pulvinar, just non rutrum lacinia, dolor magna
		        </div>
			</div>
			<div class="bubble bubble-3">
				<svg class="text" viewBox="64.7 289.8 312.7 104.8">
					<path id="curve" fill="none" d="M64.7,393.1c3.5-6.6,57.9-104.5,157.9-103.2c98.4,1.3,151,97.5,154.8,104.7"/>
		            <text width="100" fill="#fff">
		                <textPath xlink:href="#curve">
		                    0 NITRATŲ
		                </textPath>
		            </text>
		        </svg>
		        <div class="tip">
		        	Lorem ipsum dolor sit amet, con sectetur adipiscing elit. Ut tin cid unt arcu orci, tempor pulvinar nibh accumsan non. Nam pulvinar, just non rutrum lacinia, dolor magna
		        </div>
			</div>
			<div class="minerals"></div>
			<div class="circle circle-offcanvas">
				<div style="background-image: url(images/green.jpg)">
					<img src="images/bottle.png" class="water-png" alt="" />
					<div class="link-holder">
						<a href="#" class="link">Produkcija</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div class="scene-wrap scene-wrap-1">
	<?php include 'includes/lines-1.php'; ?>
</div>
<div class="layer layer-about">
	<div class="wrap">
		<h2 class="h2">
			<i class="drop"></i>
			<span>
				Ultra clean technology
			</span>
		</h2>
		<div class="mobile-lines mobile-lines-1"></div>
		<div class="circle circle-1">
			<div>
				<i style="background-image: url(images/green.jpg)"></i>
			</div>
		</div>
		<div class="circle circle-21 small">
			<div data-video-file="video/video.mp4" data-vide>
				<?php if ($detect->isMobile()): ?>
				<i style="background-image: url(images/green.jpg)"></i>
				<?php endif; ?>
			</div>
		</div>
		<div class="circle circle-3 small">
			<div data-video-file="video/video.mp4" data-vide>
				<?php if ($detect->isMobile()): ?>
				<i style="background-image: url(images/green.jpg)"></i>
				<?php endif; ?>
			</div>
		</div>
		<div class="about">
			<p>
				Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut tincidunt arcu orci, tempor pulvinar nibh accumsan non. Nam pulvinar, justo non rutrum lacinia, dolor magna maximus mauris, eu dapibus felis leo a orci. Magna maximus mauris, eu dapibus felis leo a orci. 
			</p>
			<p>
				Aenean suscipit ex quam, sed eleifend est consectetur id. In pellentesque neque non ipsum ullamcorper, et volutpat dui euismod. Integer volutpat aliquam felis sed sadgem malesuada. Nam eros mauris, luctus ut arcu ac, feugiat pulvinar mi. Nam eros mauris.
			</p>
			<div class="about-link">
				<a href="#" class="link">Skaityti daugiau</a>
			</div>
		</div>
	</div>
</div>
<div class="scene-wrap scene-wrap-9">
	<?php include 'includes/lines-3.php'; ?>
</div>
<div class="layer layer-care">
	<div class="wrap">
		<h2 class="h2">
			<i class="drop"></i>
			<span>
				Mums rūpi
			</span>
		</h2>
		<div>
			<div></div>
			<div class="mobile-lines alter mobile-lines-2"></div>
			<div class="post post-blue">
				<div class="circle big">
					<a href="#" class="sh"></a>
					<div style="background-image: url(images/green.jpg)">
					</div>
				</div>
				<div class="short">
					<div class="in">
						<a href="#" class="sh"></a>
						<h3 class="h3">
							Kiek vandens reikia išgerti per vieną dieną?
						</h3>
						<p>
							Aenean suscipit ex quam, sed eleifend est consectetur id. In pellentesque neq  dui euismod. Integer volutpat aliqua...
						</p>
					</div>
				</div>
				<div class="link-holder">
					<a href="#" class="link">Visi įrašai</a>
				</div>
				<span class="relative mobile-show drop-up">
					<i class="drop"></i>
				</span>
			</div>
		</div>
	</div>
</div>
<?php include 'includes/footer.php'; ?>