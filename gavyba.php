<?php include 'includes/header.php'; ?>
<div class="hero" style="background-image: url(images/gavyba.jpg)">
	<h1>
		Ultra clean technology
	</h1>
	<span class="relative">
		<i class="drop"></i>
	</span>
</div>
<div class="scene-wrap scene-wrap-2">
	<?php include 'includes/lines-1.php'; ?>
</div>
<div class="layer layer-taking">
	<div class="wrap">
		<div class="mobile-lines mobile-lines-3"></div>
		<div class="circle circle-1">
			<div style="background-image: url(images/water-1.jpg)">
			</div>
		</div>
		<div class="circle circle-2 small">
			<div style="background-image: url(images/water-2.jpg)">
			</div>
		</div>
		<div class="about">
			<p>
				Natūralus mineralinis vanduo "Akvilė" susiformuoja gamtos apsaugotame regione, todėl yra ypatingai švarus ir tyras – 0  nitratų. Natūralios gamtos dėka subalansuotos mineralinės sudėties galite.
			</p>
			<p>
				Mėgautis natūraliai šarminiu (pH - 8,0) ir švelnaus skonio vandeniu  „Akvilė“  kasdien. Mineralinis vanduo susiformuoja gamtos apsaugotame regione, todėl yra ypatingai švarus ir tyras – 0  nitratų. Dėka natūraliosios gamtos subalansuotos mineralinės sudėties galite mėgautis natūraliai šarminiu (pH - 8,0) ir švelnaus skonio vandeniu  „Akvilė“  kasdien. 
			</p>
		</div>
	</div>
</div>
<div>
	<div class="mobile-schema desktop-hide"></div>
	<div class="schema-close desktop-hide">
		<svg viewBox="0 0 24 24">
		    <path d="M19,6.41L17.59,5L12,10.59L6.41,5L5,6.41L10.59,12L5,17.59L6.41,19L12,13.41L17.59,19L19,17.59L13.41,12L19,6.41Z" />
		</svg>
	</div>
	<svg class="zoom__svg" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 68 110">
	  <path class="zoom__hand" d="M25.892,110c-5.183,0-11.229-2.194-17.289-9.233-7.142-8.3-9.365-16.646-6.8-25.527,1.9-6.6-.547-23.106-.572-23.272l-.048-.319.093-.309a7.364,7.364,0,0,1,3.537-4.375,6.442,6.442,0,0,1,4.945-.5c3.521,1.108,5.112,4.9,6.107,8.087l2.541,7.974L55.575,30.747a5.751,5.751,0,1,1,7.5,8.719L47.572,52.725a5.753,5.753,0,0,1,7.234,8.941L51.35,64.621a5.788,5.788,0,0,1,7.915.787,5.725,5.725,0,0,1-.623,8.1l-4.7,4.019a5.788,5.788,0,0,1,7.84.851h0a5.746,5.746,0,0,1-.61,8.112L40.215,104.409C38.622,105.77,33.089,110,25.892,110ZM4.148,51.884c.363,2.526,2.376,17.428.441,24.152C2.279,84.023,4.252,91.284,10.8,98.89c12.362,14.36,24.2,6.18,27.532,3.33L59.289,84.3a2.868,2.868,0,0,0,.3-4.047h0a2.851,2.851,0,0,0-1.96-.986,2.82,2.82,0,0,0-2.087.677l-5.667,4.844a2.882,2.882,0,0,1-3.76-4.368l10.646-9.1a2.847,2.847,0,0,0,.311-4.034,2.881,2.881,0,0,0-4.047-.3L44.567,74.21a2.884,2.884,0,1,1-3.761-4.372L52.921,59.477a2.865,2.865,0,0,0-3.738-4.343L37.064,65.5A2.883,2.883,0,0,1,33.3,61.128L61.2,37.277a2.864,2.864,0,1,0-3.736-4.342L16.977,67.549,13.109,55.415c-.726-2.325-1.949-5.489-4.207-6.2a3.548,3.548,0,0,0-2.721.292A4.428,4.428,0,0,0,4.148,51.884Z"></path>
	  <polygon class="zoom__arrow zoom__arrow--next" points="59.262 0 56.879 2.373 61.999 7.949 34 7.949 34 11.33 61.999 11.33 56.879 16.906 59.262 19.278 68 9.639 59.262 0"></polygon>
	  <polygon class="zoom__arrow zoom__arrow--prev" points="34 7.949 6.001 7.949 11.121 2.373 8.738 0 0 9.639 8.738 19.278 11.121 16.906 6.001 11.33 34 11.33 34 7.949"></polygon>
	</svg>
	<div class="schema-pop desktop-hide">
		<div>
			<img src="img/schema.png" alt="">
		</div>
	</div>
	<div class="schema mobile-hide">
		<i></i>
		<div class="schema-circle schema-circle-main"><b></b></div>
		<div class="schema-circle schema-circle-1"><b></b></div>
		<div class="schema-circle schema-circle-2"><b></b></div>
		<div class="schema-circle schema-circle-3"><b></b></div>
		<div class="txt txt-1">
			Iš miškingiausios<br> Lietuvoje saugomos<br> teritorijos
		</div>
		<div class="txt txt-2">
			Uždara<br> sistema
		</div>
		<div class="txt txt-3">
			Filtruojamas į sistemą <br>patenkantis oras
		</div>
		<div class="txt txt-4">
			Butelių ir kamštelių <br>sterilizavimas
		</div>
		<div class="txt txt-5">
			Išpilstoma <br>be sąlyčio
		</div>
	</div>
</div>
<div class="layer layer-water">
	<div class="wrap">
		<h2 class="h2">
			<i class="drop"></i>
			<span>
				Vandens gavyba
			</span>
		</h2>
		<div class="mobile-lines mobile-lines-4"></div>
		<div class="circle circle-1">
			<div style="background-image: url(images/water-3.jpg)">
			</div>
		</div>
		<div class="circle circle-2 small">
			<div style="background-image: url(images/water-4.jpg)">
			</div>
		</div>
		<div class="about">
			<p>
				Natūralus mineralinis vanduo "Akvilė" susiformuoja gamtos apsaugotame regione, todėl yra ypatingai švarus ir tyras – 0  nitratų. Natūralios gamtos dėka subalansuotos mineralinės sudėties galite.
			</p>
			<p>
				Mėgautis natūraliai šarminiu (pH - 8,0) ir švelnaus skonio vandeniu  „Akvilė“  kasdien. Mineralinis vanduo susiformuoja gamtos apsaugotame regione, todėl yra ypatingai švarus ir tyras – 0  nitratų. Dėka natūraliosios gamtos subalansuotos mineralinės sudėties galite mėgautis natūraliai šarminiu (pH - 8,0) ir švelnaus skonio vandeniu  „Akvilė“  kasdien. 
			</p>
		</div>
	</div>
</div>
<div class="scene-wrap scene-wrap-13">
	<?php include 'includes/lines-3.php'; ?>
</div>
<div class="scene-wrap scene-wrap-3">
	<?php include 'includes/lines-1.php'; ?>
</div>
<div class="layer layer-long">
	<div class="wrap">
		<h2 class="h2 small">
			<i class="drop"></i>
			<span>
				Vanduo "Akvilė" susiformuoja gamtos apsaugotame regione, todėl yra ypatingai švarus ir tyras - 0 nitratų.
			</span>
		</h2>
		<div class="mobile-lines mobile-lines-5"></div>
		<div class="video">
			<div class="in">
				<a class="play" href="video/video.mp4"></a>
				<div data-video-file="video/video.mp4" class="video-container"></div>
				<i style="background-image: url(images/video.jpg)"></i>
			</div>
		</div>
		<div class="about centered">
			<p>
				Aenean suscipit ex quam, sed eleifend est consectetur id. In pellentesque neq  dui euismod. Integer volutpat aliqua... felis sed sadgem malesuada. Nam eros mauris, luctus ut arcu ac, feugiat pulvinar mi. Aenean suscipit ex quam, sed eleifend est consectetur id. In pellentesque neq  dui euismod. Integer volutpat aliqua... felis sed sadgem malesuada. Nam eros mauris, luctus ut arcu ac, feugiat pulvinar mi.
			</p>
			<p>
				Aenean suscipit ex quam, sed eleifend est consectetur id. In pellentesque neq  dui euismod. Integer volutpat aliqua... felis sed sadgem malesuada. Nam eros mauris, luctus ut arcu ac, feugiat pulvinar mi. Aenean suscipit ex quam, sed eleifend est consectetur id. In pellentesque neq  dui euismod. Integer volutpat aliqua... felis sed sadgem malesuada. Nam eros mauris, luctus ut arcu ac, feugiat pulvinar mi.
			</p>
		</div>
	</div>
</div>
<?php include 'includes/footer.php'; ?>